import { Component } from '@angular/core';
import { NavController, Platform } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { VisionServiceProvider } from '../../providers/vision-service/vision-service';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  //photo object
  myphoto:any;
  //google answer
  mylabels:any;
  //Help to deconstruct the google answer
  result:any;
  myresponse:any;
  //Local JSON didn't work and I didn't find any interessting API, so i'm doing this
  myItem: Array<String> = ['Human', 'Furniture', 'Car', 'Bag', 'Animal', 'Food', 'Boat', 'Tree', 'Water', 'Sky', 
  'Electronics', 'Work', 'Smile', 'Angry', 'Cat', 'Dog', 'Shoes', 'Clothes', 'Hair', 'Computer', 'Wood', 'Metal', 
  'Glasses', 'Bottle', 'Screen', 'Piano', 'Face', 'Leg', 'Hand', 'Finger', 'Beard', 'Interaction'];
  //The random object to take
  randomItem:any;

  //Time variable
  seconds:any;
  secondsRemaining: any;
  timer:any;
  //The timer should only be initialised once, otherwise multiple instance
  //Will be created and the seconds will decrease too fast
  flagTimeInitilized:boolean = false;

  //Player status
  win:any;
  loose:any;

  //To mask the HTML when the fun is over
  overlayHidden: boolean = true;

  constructor(public platform: Platform,
              public navCtrl: NavController,
              private camera: Camera,
              private vision: VisionServiceProvider) {
  }

  //Start the timer
  ngOnInit() {
    this.initTimer();
  }

  //Set the timer variable
  initTimer() {
    if (!this.seconds) {
      //The time in the entire procces (for multiple party)
      this.seconds = 10000;
      //the seconds displayed on screen for each party
      this.secondsRemaining = 100;
    }

    this.timer = <any>{
      seconds: this.seconds,
      runTimer: false,
      hasStarted: false,
      hasFinished: false,
      //Help us temporise when we won but don't want to be on the winner screen too fast
      hasToFinish: 0,
      secondsRemaining: this.secondsRemaining
    };
    this.timer.displayTime = this.timer.secondsRemaining;
    this.startTimer()
  }
  //Start counting
  startTimer() {
    this.timer.secondsRemaining = 100;
    this.seconds = 10000;
    this.timer.hasStarted = true;
    this.timer.runTimer = true;
    if (this.flagTimeInitilized == false){
      this.timerTick();
      this.flagTimeInitilized = true;
    }    
  }
  //For stopping the timer
  pauseTimer() {
    this.timer.runTimer = false;
  }
  //the time line, help us define controls to win or loose during a round
  timerTick(){
    setTimeout(() => {
      if (!this.timer.runTimer) {
        this.timer.seconds--;
        if (this.timer.seconds == this.timer.hasToFinish){
          this.overlayHidden = false;
          this.win = true;
        }
        else{
          this.timerTick();
        }
      }
      else{
        if (this.timer.seconds == this.timer.hasToFinish){
          this.overlayHidden = false;
          this.win = true;
        }
        this.timer.secondsRemaining--;
        this.timer.seconds--;
        this.timer.displayTime = this.timer.secondsRemaining;
        if (this.timer.secondsRemaining > 0) {
          this.timerTick();
        } 
        else {
          this.timer.hasFinished = true;
          this.overlayHidden = false;
          this.loose = true;
        }
      }
    }, 1000);
  }
  //a boolean speaking for himself
  hasFinished() {
    return this.timer.hasFinished;
  }
  //Take a random item from the list above
  pickItem(){
    this.randomItem = this.myItem[this.getRandomInt(this.myItem.length)]
    console.log(this.randomItem);
  }
  //help us taking photo and send it to google, and gather the answer in mylabels
  takePhoto(){
    const options: CameraOptions = {
      quality: 70,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      targetHeight: 200
    }
    this.camera.getPicture(options).then((imageData) => {
      this.vision.getLabels(imageData).subscribe((result) => { 
        this.myresponse = result;
        this.mylabels = this.myresponse.responses[0].labelAnnotations;
        this.myphoto= "data:image/jpeg;base64," + imageData;
        this.testPhoto();
      });  
    });
  }
  //do the same thing but by browsing the picture in the smartphone
  getPhotoLibrary(){
    const options: CameraOptions = {
      quality: 70,
      destinationType: this.camera.DestinationType.DATA_URL,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      saveToPhotoAlbum: false,
      targetHeight: 200
    }
    this.camera.getPicture(options).then((imageData) => {
      this.vision.getLabels(imageData).subscribe((result) => { 
        this.myresponse = result;
        this.mylabels = this.myresponse.responses[0].labelAnnotations;
        this.myphoto= "data:image/jpeg;base64," + imageData;
        this.testPhoto();
      });  
    });
  }
  //Try to see if the randomItem is in one element of google's answer
  testPhoto(){
    var matchFound = false;
    var list = [];
    for(var i = 0; i < this.mylabels.length; i++){
      if(this.randomItem.indexOf(this.mylabels[i].description) !== -1){
        matchFound = true;
        list.push(this.mylabels[i]);
      }
    }
    if(matchFound){
      this.mylabels = list;
      this.timer.hasToFinish = this.timer.seconds - 3;
    }
  }

  //Gives us a random int between 0 and max-1
  getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
  }

  //Mask some element of the html
  public hideOverlay() {
    this.overlayHidden = false;
  }

  //The reset function to make an other party anytime or after a success win or an epic failure
  reset(){
    this.myphoto = null;
    this.mylabels = null;
    this.result = null;
    this.myresponse = null;
    this.randomItem = null;
    this.win = null;
    this.loose = null;
    this.seconds = null;
    this.overlayHidden = true;
    this.timer.secondsRemaining = 100;
    this.seconds = 10000;
  }

  //Sadly, saying good bye to the beautifull app :(
  quit(){
    this.platform.exitApp();
  }
}
