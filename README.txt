////////////////  Description


L'application a pour but de demander la prise en photo d'un objet random.
(l'image peux tout aussi bien être prise de la galerie photo)

Une fois cet objet random pris en photo gâce au plugin camera, 
l'application verifie avec l'API  google cloud vision 
si cela correspond bien a ce qui était attendu.
Tout cela dans une durée impartie de 100 secondes affichées à l'écran.

Pour ce qui concerne les objets random, pas d'API interessantes n'étaient disponibles
et je n'ai pas réussi, malgrés le cours, à exploiter un JSON local, 
j'ai donc créer une liste d'objets random. Le code est cependant bien documenté.

Les fonctions liés au temps ont toutes été codés manuellement puisque je n'ai pas trouvé la doc d'un 
time me permettant de faire des delais ou d'afficher le temps par ticks.

Enfin pour des raisons financiaire évidente, je couperais la facturation avec l'API google
la semaine prochaine.


//////////////////  Les screenshots


J'ai mis les screenshots dans un ordre logique.
Une page et un bouton afin d'initialiser une partie, une fois lancé, un objet et 
une durée s'affiche et la partie commence.
Le bouton en haut à gauche permet de recommencer et reset toutes les variables 
(ainsi que l'affichage).

Le bouton en bas à gauche permet d'acceder aux photos de la galerie
et le bouton en bas à droite de prendre une photo

Une fois la photo envoyée à google, un retour apparait.
Si un des libellés contient l'objet à prendre en photo, il aparait seul tout en haut.
Ou bout de 5 secondes, l'affichage pour le vainqueur apparait.

En revanche, si aucun libellé ne contient l'objet recherché, toute la liste est affiché
afin de permettre au jour de voir ce que contienne les libellés et peux prendre une autre
photo.

Si à la fin des 100 secondes, aucune photo ne correspond, la partie est perdu.

L'APK ce trouve ici :
\CHAIBDOUR_Malik_m2fa_ionic\platforms\android\app\build\outputs\apk\debug\
